.. |app-manager-update| image:: /screenshots/app-manager/app-manager-update.png
   :scale: 60%
   :align: bottom
   :alt: App manager update

.. |app-manager-update-not-available| image:: /screenshots/app-manager/app-manager-update-not-available.png
   :scale: 60%
   :align: bottom
   :alt: App manager update not available

.. |app-manager-uninstall| image:: /screenshots/app-manager/app-manager-uninstall.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall

.. |app-manager-install| image:: /screenshots/app-manager/app-manager-install.png
   :scale: 60%
   :align: bottom
   :alt: App manager install

.. |app-manager-install-list| image:: /screenshots/app-manager/app-manager-install-list.png
   :scale: 60%
   :align: bottom
   :alt: App manager install list

.. |app-manager-main| image:: /screenshots/app-manager/app-manager-main.png
   :scale: 60%
   :align: bottom
   :alt: App manager main

.. |app-manager-menu| image:: /screenshots/app-manager/app-manager-menu.png
   :scale: 60%
   :align: bottom
   :alt: App manager menu

.. |app-manager-update-menu| image:: /screenshots/app-manager/app-manager-update-menu.png
   :scale: 60%
   :align: bottom
   :alt: App manager update menu

.. |app-manager-update-prompt| image:: /screenshots/app-manager/app-manager-update-prompt.png
   :scale: 60%
   :align: bottom
   :alt: App manager update prompt

.. |app-manager-update-all-summary| image:: /screenshots/app-manager/app-manager-update-all-summary.png
   :scale: 60%
   :align: bottom
   :alt: App manager update all summary

.. |app-manager-update-confirm| image:: /screenshots/app-manager/app-manager-update-confirm.png
   :scale: 60%
   :align: bottom
   :alt: App manager confirm

.. |app-manager-update-app-summary| image:: /screenshots/app-manager/app-manager-update-app-summary.png
   :scale: 60%
   :align: bottom
   :alt: App manager update app summary

.. |app-manager-update-size| image:: /screenshots/app-manager/app-manager-update-size.png
   :scale: 60%
   :align: bottom
   :alt: App manager update size

.. |app-manager-update-pending| image:: /screenshots/app-manager/app-manager-update-pending.png
   :scale: 60%
   :align: bottom
   :alt: App manager update pending

.. |app-manager-update-progress| image:: /screenshots/app-manager/app-manager-update-progress.png
   :scale: 60%
   :align: bottom
   :alt: App manager update progress

.. |app-manager-update-success| image:: /screenshots/app-manager/app-manager-update-success.png
   :scale: 60%
   :align: bottom
   :alt: App manager update success

.. |app-manager-update-none| image:: /screenshots/app-manager/app-manager-update-none.png
   :scale: 60%
   :align: bottom
   :alt: App manager update none

.. |app-manager-catalogs| image:: /screenshots/app-manager/app-manager-catalogs.png
   :scale: 60%
   :align: bottom
   :alt: App manager update catalogs

.. |app-manager-catalog-new| image:: /screenshots/app-manager/app-manager-catalog-new.png
   :scale: 60%
   :align: bottom
   :alt: App manager update catalog new

.. |app-manager-catalog-read-only| image:: /screenshots/app-manager/app-manager-catalog-read-only.png
   :scale: 60%
   :align: bottom
   :alt: App manager update catalog read-only

.. |app-manager-log| image:: /screenshots/app-manager/app-manager-log.png
   :scale: 60%
   :align: bottom
   :alt: App manager log

.. |app-manager-log-export| image:: /screenshots/app-manager/app-manager-log-export.png
   :scale: 60%
   :align: bottom
   :alt: App manager log export

.. |app-manager-log-export-folder| image:: /screenshots/app-manager/app-manager-log-export-folder.png
   :scale: 60%
   :align: bottom
   :alt: App manager log export folder

.. |app-manager-log-export-folder-new| image:: /screenshots/app-manager/app-manager-log-export-folder-new.png
   :scale: 60%
   :align: bottom
   :alt: App manager log export folder new

.. |app-manager-update-check| image:: /screenshots/app-manager/app-manager-update-check.png
   :scale: 60%
   :align: bottom
   :alt: App manager update check

.. |app-manager-install-confirm| image:: /screenshots/app-manager/app-manager-install-confirm.png
   :scale: 60%
   :align: bottom
   :alt: App manager install confirm

.. |app-manager-install-app-summary| image:: /screenshots/app-manager/app-manager-install-app-summary.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app summary

.. |app-manager-install-app-description| image:: /screenshots/app-manager/app-manager-install-app-description.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app description

.. |app-manager-install-app-size| image:: /screenshots/app-manager/app-manager-install-app-size.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app size

.. |app-manager-install-preparation| image:: /screenshots/app-manager/app-manager-install-preparation.png
   :scale: 60%
   :align: bottom
   :alt: App manager install preparations

.. |app-manager-install-app-1| image:: /screenshots/app-manager/app-manager-install-app-1.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app 1

.. |app-manager-install-app-2| image:: /screenshots/app-manager/app-manager-install-app-2.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app 2

.. |app-manager-install-app-3| image:: /screenshots/app-manager/app-manager-install-app-3.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app 3

.. |app-manager-install-app-4| image:: /screenshots/app-manager/app-manager-install-app-4.png
   :scale: 60%
   :align: bottom
   :alt: App manager install app 4

.. |app-manager-app-installed| image:: /screenshots/app-manager/app-manager-app-installed.png
   :scale: 60%
   :align: bottom
   :alt: App manager app installed

.. |app-manager-uninstall-confirm| image:: /screenshots/app-manager/app-manager-uninstall-confirm.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall confirm

.. |app-manager-uninstall-summary| image:: /screenshots/app-manager/app-manager-uninstall-summary.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall summary

.. |app-manager-uninstall-description| image:: /screenshots/app-manager/app-manager-uninstall-description.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall description

.. |app-manager-uninstall-size| image:: /screenshots/app-manager/app-manager-uninstall-size.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall size

.. |app-manager-uninstall-app-1| image:: /screenshots/app-manager/app-manager-uninstall-app-1.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall app 1

.. |app-manager-uninstall-app-2| image:: /screenshots/app-manager/app-manager-uninstall-app-2.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall app 2

.. |app-manager-uninstall-success| image:: /screenshots/app-manager/app-manager-uninstall-success.png
   :scale: 60%
   :align: bottom
   :alt: App manager uninstall success

.. |general-application-manager| image:: /screenshots/app-manager-ui/general_application_manager.png
   :width: 20px
   :align: bottom
   :alt: General Application manager

