.. |navigation-maep-main| image:: /screenshots/navigation/navigation-maep-main.png
   :scale: 60%
   :align: bottom
   :alt: Maep main

.. |navigation-maep-track| image:: /screenshots/navigation/navigation-maep-track.png
   :scale: 60%
   :align: bottom
   :alt: Maep track

.. |navigation-maep-menu| image:: /screenshots/navigation/navigation-maep-menu.png
   :scale: 60%
   :align: bottom
   :alt: Maep menu

.. |navigation-maep-about| image:: /screenshots/navigation/navigation-maep-about.png
   :scale: 60%
   :align: bottom
   :alt: Maep about

.. |navigation-maep-track-menu| image:: /screenshots/navigation/navigation-maep-track-menu.png
   :scale: 60%
   :align: bottom
   :alt: Maep track menu

.. |navigation-maep-track-import| image:: /screenshots/navigation/navigation-maep-track-import.png
   :scale: 60%
   :align: bottom
   :alt: Maep track import

.. |navigation-maep-search| image:: /screenshots/navigation/navigation-maep-search.png
   :scale: 60%
   :align: bottom
   :alt: Maep search

.. |navigation-maep-side-panel| image:: /screenshots/navigation/navigation-maep-side-panel.png
   :scale: 60%
   :align: bottom
   :alt: Maep side panel

.. |navigation-maep-track-export| image:: /screenshots/navigation/navigation-maep-track-export.png
   :scale: 60%
   :align: bottom
   :alt: Maep track export

.. |navigation-maep-side-buttons| image:: /screenshots/navigation/navigation-maep-side-buttons.png
   :scale: 60%
   :align: bottom
   :alt: Maep side buttons

.. |navigation-maep-side-buttons-inactive| image:: /screenshots/navigation/navigation-maep-side-buttons-inactive.png
   :scale: 60%
   :align: bottom
   :alt: Maep side buttons inactive

.. |navigation-gpsrecorder-resume| image:: /screenshots/navigation/navigation-gpsrecorder-resume.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder resume

.. |navigation-gpsrecorder-pause| image:: /screenshots/navigation/navigation-gpsrecorder-pause.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder pause

.. |navigation-gpsrecorder-pause-name| image:: /screenshots/navigation/navigation-gpsrecorder-pause-name.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder pause name

.. |navigation-gpsrecorder-point| image:: /screenshots/navigation/navigation-gpsrecorder-point.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder point

.. |navigation-gpsrecorder-point-name| image:: /screenshots/navigation/navigation-gpsrecorder-point-name.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder point name

.. |navigation-gpsrecorder-means-of-transport| image:: /screenshots/navigation/navigation-gpsrecorder-means-of-transport.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder means of transport

.. |navigation-gpsrecorder-transport-1| image:: /screenshots/navigation/navigation-gpsrecorder-transport-1.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 1

.. |navigation-gpsrecorder-transport-2| image:: /screenshots/navigation/navigation-gpsrecorder-transport-2.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 2

.. |navigation-gpsrecorder-transport-3| image:: /screenshots/navigation/navigation-gpsrecorder-transport-3.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 3

.. |navigation-gpsrecorder-transport-4| image:: /screenshots/navigation/navigation-gpsrecorder-transport-4.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 4

.. |navigation-gpsrecorder-transport-5| image:: /screenshots/navigation/navigation-gpsrecorder-transport-5.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 5

.. |navigation-gpsrecorder-transport-6| image:: /screenshots/navigation/navigation-gpsrecorder-transport-6.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 6

.. |navigation-gpsrecorder-transport-7| image:: /screenshots/navigation/navigation-gpsrecorder-transport-7.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 7

.. |navigation-gpsrecorder-transport-8| image:: /screenshots/navigation/navigation-gpsrecorder-transport-8.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 8

.. |navigation-gpsrecorder-transport-9| image:: /screenshots/navigation/navigation-gpsrecorder-transport-9.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder transport 9

.. |navigation-new-track-name| image:: /screenshots/navigation/navigation-new-track-name.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder new track name

.. |navigation-new-track-file| image:: /screenshots/navigation/navigation-new-track-file.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder new track file

.. |navigation-gpsrecorder-settings-1| image:: /screenshots/navigation/navigation-gpsrecorder-settings-1.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder settings 1

.. |navigation-gpsrecorder-settings-2| image:: /screenshots/navigation/navigation-gpsrecorder-settings-2.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder settings 2

.. |navigation-gpsrecorder-menu| image:: /screenshots/navigation/navigation-gpsrecorder-menu.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder menu

.. |navigation-gpsrecorder-log-step-1| image:: /screenshots/navigation/navigation-gpsrecorder-log-step-1.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder log step 1

.. |navigation-gpsrecorder-log-step-2| image:: /screenshots/navigation/navigation-gpsrecorder-log-step-2.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder log step 2

.. |navigation-gpsrecorder-log-step-3| image:: /screenshots/navigation/navigation-gpsrecorder-log-step-3.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder log step 3

.. |navigation-gpsrecorder-log-step-4| image:: /screenshots/navigation/navigation-gpsrecorder-log-step-4.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder log step 4

.. |navigation-gpsrecorder-main| image:: /screenshots/navigation/navigation-gpsrecorder-main.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder main

.. |navigation-gpsrecorder-sat| image:: /screenshots/navigation/navigation-gpsrecorder-sat.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder SAT

.. |navigation-gpsrecorder-spd| image:: /screenshots/navigation/navigation-gpsrecorder-spd.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder SPD

.. |navigation-gpsrecorder-cancel| image:: /screenshots/navigation/navigation-gpsrecorder-cancel.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder cancel

.. |navigation-gpsrecorder-stopped| image:: /screenshots/navigation/navigation-gpsrecorder-stopped.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder stopped

.. |navigation-gpsrecorder-paused| image:: /screenshots/navigation/navigation-gpsrecorder-paused.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder paused

.. |navigation-gpsrecorder-recording| image:: /screenshots/navigation/navigation-gpsrecorder-recording.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder recording

.. |navigation-gpsrecorder-button-start| image:: /screenshots/navigation/navigation-gpsrecorder-button-start.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button start

.. |navigation-gpsrecorder-button-pause| image:: /screenshots/navigation/navigation-gpsrecorder-button-pause.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button pause

.. |navigation-gpsrecorder-button-waypoint| image:: /screenshots/navigation/navigation-gpsrecorder-button-waypoint.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button waypoint

.. |navigation-gpsrecorder-button-transport| image:: /screenshots/navigation/navigation-gpsrecorder-button-transport.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button transport

.. |navigation-gpsrecorder-button-sat| image:: /screenshots/navigation/navigation-gpsrecorder-button-sat.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button SAT

.. |navigation-gpsrecorder-button-spd| image:: /screenshots/navigation/navigation-gpsrecorder-button-spd.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button SPD

.. |navigation-gpsrecorder-button-cell| image:: /screenshots/navigation/navigation-gpsrecorder-button-cell.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder button CELL

.. |navigation-unit-system| image:: /screenshots/navigation/navigation-unit-system.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder unit system

.. |navigation-gpsrecorder-speed-unit| image:: /screenshots/navigation/navigation-gpsrecorder-speed-unit.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder speed unit

.. |navigation-gpsrecorder-convert-1| image:: /screenshots/navigation/navigation-gpsrecorder-convert-1.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder convert 1

.. |navigation-gpsrecorder-convert-2| image:: /screenshots/navigation/navigation-gpsrecorder-convert-2.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder convert 2

.. |navigation-gpsrecorder-convert-3| image:: /screenshots/navigation/navigation-gpsrecorder-convert-3.png
   :scale: 60%
   :align: bottom
   :alt: GPS Recorder convert 3

.. |navigation-cloudgps-main| image:: /screenshots/navigation/navigation-cloudgps-main.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS main

.. |navigation-cloudgps-3d| image:: /screenshots/navigation/navigation-cloudgps-3d.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS 3D

.. |navigation-cloudgps-button-zoom-out| image:: /screenshots/navigation/navigation-cloudgps-button-zoom-out.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button zoom out

.. |navigation-cloudgps-button-zoom-in| image:: /screenshots/navigation/navigation-cloudgps-button-zoom-in.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button zoom in

.. |navigation-cloudgps-button-3d| image:: /screenshots/navigation/navigation-cloudgps-button-3d.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button 3D

.. |navigation-cloudgps-button-2d| image:: /screenshots/navigation/navigation-cloudgps-button-2d.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button 2D

.. |navigation-cloudgps-button-compass| image:: /screenshots/navigation/navigation-cloudgps-button-compass.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button compass

.. |navigation-cloudgps-button-binoculars| image:: /screenshots/navigation/navigation-cloudgps-button-binoculars.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button binoculars

.. |navigation-cloudgps-button-dot| image:: /screenshots/navigation/navigation-cloudgps-button-dot.png
   :scale: 60%
   :align: bottom
   :alt: CloudGPS button dot

