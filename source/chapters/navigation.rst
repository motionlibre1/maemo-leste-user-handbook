 .. include:: ../chapters-img/navigation-img.rst

Navigation
==========

.. note:: Using the applications in this category, you can browse maps, get your current geographical position, as calculated by the GPS, and depending on each application, use other features such as seeing your position live on the map, tracking your movement, or saving or importing tracking graphs.

Maep
----

Maep is a navigation app which allows you to see your current position and movement live on an online map, record your movement in a track file, or import an already existing (recorded) track file.

.. note:: This application is not installed by default. :ref:`chapters/appmanager:Install` it from the :ref:`chapters/appmanager:Application Manager`.

|navigation-maep-main|

You can slide your finger on the screen to scroll on the map, and pinch to zoom.

There are also several buttons, on the sides of the screen:

     |navigation-maep-side-buttons|

To the left, there are three buttons:

   #. **\- Zoom out**
   #. **Current position acquired by the GPS** - If you tap on this button, the map will be focused on the current location. If this button is grayed-out, then the :term:`GPS` didn't lock the position yet.

          |navigation-maep-side-buttons-inactive|

   #. **\+ Zoom in**

     |navigation-maep-side-panel|

To the right, there's a right pointing arrow button, which toggles showing a side panel, from which you can select the source of the maps being displayed.

   #. **OpenStreetMap I**
   #. **OpenStreetMap II**
   #. **OpenCycleMap**
   #. **Public Transport**
   #. **Google Maps**
   #. **Virtual Earth**
   #. **Virtual Earth Satellite**
   #. **Virtual Earth Hybrid**

Here there's also another option:

#. **Double Pixel**

Additional settings and options are in the :ref:`chapters/ui:In-app menu`:

     |navigation-maep-menu|

#. :ref:`About <chapters/navigation:Maep About>`
#. :ref:`Track <chapters/navigation:Maep Track>`
#. :ref:`Search <chapters/navigation:Maep Search>`
#. :ref:`Wikipedia <chapters/navigation:Maep Wikipedia>`
#. :ref:`Heart Rate <chapters/navigation:Maep Heart Rate>`
#. :ref:`Screen Rotation <chapters/navigation:Maep Screen Rotation>`

Maep About
""""""""""

Here there is information about the application, such as a short description, the version, and a link to the project's webpage.

     |navigation-maep-about|

There are more tabs, other than the main one, which show even more details.

On the bottom of the screen there are some buttons, which you you can tap on, to switch to these tabs, and see the corresponding details:

   #. **Copyright**
   #. **License**
   #. **Authors**
   #. **Donate**
   #. **Bugs**

Maep Track
""""""""""

This shows the *Track* menu, from where you can pick the following options which enable you recording or loading prerecorded tracks:

     |navigation-maep-track-menu|

You have the following options:

   #. **Capture** - Checking this option will start tracking the device's movement, and as it moves, it will leave a red trail on the map. Unchecking will stop tracking.

          |navigation-maep-track|

   #. **Clear** - Clear the recorded track, to start recording a new track.

   #. **Import** - Import a recorded track from a file. Navigate through the folders to find the needed file.

          |navigation-maep-track-import|

   #. **Export** - Export the current recorded track to a file. Tap on *Location* to select which folder the file should be saved to.

          |navigation-maep-track-export|

Maep Search
"""""""""""

     |navigation-maep-search|

Maep Wikipedia
""""""""""""""

.. note:: :ref:`summary:Unexpected behavior`: This doesn't seem to work.

Maep Heart Rate
"""""""""""""""

.. note:: Untested feature

Maep Screen Rotation
""""""""""""""""""""

.. note:: :ref:`summary:Unexpected behavior`: This doesn't seem to work.

GPS Recorder
------------

GPS Recorder is a navigation app which allows you to record your current position and movement in a track file.


.. note:: This application is not installed by default. :ref:`chapters/appmanager:Install` it from the :ref:`chapters/appmanager:Application Manager`.

|navigation-gpsrecorder-main|

In the main window, there is the following information:

#. **Status** - Recorder/GPS status
#. **Fields** - Types of information stored in the track file
#. **Mode** - 2D/3D location type
#. **Time** - Current date and time
#. **SatUse** - Number of satellites used in the fix / satellites available

#. **Lat** - Latitude component of the current location
#. **Long** - Longitude component of the current location
#. **Alt** - Altitude component of the current location
#. **Track**
#. **Speed** - Current speed

#. **Distance** - Recorded distance
#. **Time setup**
#. **Waypoints** - Waypoints recorded
#. **Last waypoint** - Last waypoint's name

#. **HorizEP**
#. **Transport** - Type of transport set when started recording
#. **Battery** - Device battery status

At the bottom of the screen, there are the following buttons/indicators:

#. |navigation-gpsrecorder-stopped| |navigation-gpsrecorder-recording| |navigation-gpsrecorder-paused| :ref:`Status indicator <chapters/navigation:GPS Recorder Status indicator>`
#. |navigation-gpsrecorder-button-start| |navigation-gpsrecorder-button-pause| :ref:`Start/Pause button <chapters/navigation:GPS Recorder Start/Pause button>`
#. |navigation-gpsrecorder-button-waypoint| :ref:`Waypoint button <chapters/navigation:GPS Recorder Waypoint button>`
#. |navigation-gpsrecorder-button-transport| :ref:`Transport button <chapters/navigation:GPS Recorder Transport button>`
#. |navigation-gpsrecorder-button-sat| :ref:`SAT button <chapters/navigation:GPS Recorder SAT button>`
#. |navigation-gpsrecorder-button-spd| :ref:`SPD button <chapters/navigation:GPS Recorder SPD button>`
#. |navigation-gpsrecorder-button-cell| :ref:`CELL button <chapters/navigation:GPS Recorder CELL button>`

There are also other options, in the :ref:`chapters/ui:In-app menu`:

     |navigation-gpsrecorder-menu|

#. :ref:`Start <chapters/navigation:GPS Recorder Start/Stop button>`
#. :ref:`Settings <chapters/navigation:GPS Recorder Settings>`
#. :ref:`Convert <chapters/navigation:GPS Recorder Convert>`
#. :ref:`About <chapters/navigation:GPS Recorder About>`

GPS Recorder Status indicator
"""""""""""""""""""""""""""""

This shows if the track is being currently recorded, or not:

     |navigation-gpsrecorder-stopped| - The recording is stopped. To start it, tap on the :ref:`Start/Pause button <chapters/navigation:GPS Recorder Start/Pause button>`, or on the :ref:`Start/Stop button <chapters/navigation:GPS Recorder Start/Stop button>` in the :ref:`chapters/ui:In-app menu`.

     |navigation-gpsrecorder-recording| - The recording is running. To stop it, tap on the :ref:`Start/Pause button <chapters/navigation:GPS Recorder Start/Pause button>` to pause it, or tap on the :ref:`Start/Stop button <chapters/navigation:GPS Recorder Start/Stop button>`.

     |navigation-gpsrecorder-paused| - The recording is paused. To resume it, tap on the :ref:`Start/Pause button <chapters/navigation:GPS Recorder Start/Pause button>`.

GPS Recorder Start/Pause button
"""""""""""""""""""""""""""""""

When the recording is stopped, or not started yet, you can start it from here.

     |navigation-gpsrecorder-button-start|

You can also start recording by tapping the :ref:`Start/Stop button <chapters/navigation:GPS Recorder Start/Stop button>` in the :ref:`chapters/ui:In-app menu`.

If you start recording again, you will be asked if to save to a new track file, or to append to an existing one:

     |navigation-new-track-file|

Options to choose:

#. **Cancel** - Do not start recording
#. **No** - Append to an existing track file
#. **Yes** - Create a new track file

          |navigation-new-track-name|

          You will have to pick a name for the new file, and the Means of transport for this track (the same options as when tapping on the :ref:`Transport button <chapters/navigation:GPS Recorder Transport button>`).

When the recording runs, the button changes. and it looks like this.

     |navigation-gpsrecorder-button-pause|

Tapping on it while the recording is running, will pause recording (any movement or change of position won't be recorded during this time).

You can choose a name for this pause/break point:

     |navigation-gpsrecorder-pause-name|

The following message will be then displayed, confirming that the recording has been paused (the chosen name for this pause is written in brackets).

     |navigation-gpsrecorder-pause|

When the recording is paused, the button will look again as before, allowing you to tap on it again to resume recording.

     |navigation-gpsrecorder-button-start|

The following message will be then displayed, confirming that the recording has been resumed.

     |navigation-gpsrecorder-resume|

GPS Recorder Waypoint button
""""""""""""""""""""""""""""

During recording, you can save the current location as a waypoint of the track by tapping on the following button:

     |navigation-gpsrecorder-button-waypoint|

You can choose a name for this waypoint:

     |navigation-gpsrecorder-point-name|

The following message will be then displayed, confirming that the waypoint has been saved (snapped).

     |navigation-gpsrecorder-point|

GPS Recorder Transport button
"""""""""""""""""""""""""""""

     |navigation-gpsrecorder-button-transport|

Here you can choose the type of transport being used for the current track.

     |navigation-gpsrecorder-means-of-transport|

Tap on *Means of transport*, and a (long and exhaustive) list of transport types will be shown:

     |navigation-gpsrecorder-transport-1|

     |navigation-gpsrecorder-transport-2|

     |navigation-gpsrecorder-transport-3|

     |navigation-gpsrecorder-transport-4|

     |navigation-gpsrecorder-transport-5|

     |navigation-gpsrecorder-transport-6|

     |navigation-gpsrecorder-transport-7|

     |navigation-gpsrecorder-transport-8|

     |navigation-gpsrecorder-transport-9|

Options available:

*N/A*, *Airship*, *Balloon*, *Barge*, *Bike*, *Boat*, *Bus*, *Calash*, *Camel*, *Car*, *Cargo*, *Deltaplane*, *Donkey*,

*Ferry*, *Foot*, *Free fall*, *Glider*, *Gyrocopter*, *Helicopter*, *Horse*, *Jet ski*, *Motorbike*, *Paraglider*,

*Plane*, *Pony*, *Roller*, *Sailing Boat*, *Scooter*, *Seaplane*, *Ship*, *Skateboard*, *Ski*, *Snowboard*, *Subway*,

*Sulky*, *Swimming*, *Taxi*, *Train*, *Tramway*, *Truck*, *Tuk Tuk*, *Ultralight*, *Zeppelin*, *Other : ?*

GPS Recorder SAT button
"""""""""""""""""""""""

     |navigation-gpsrecorder-button-sat|

If you tap on this button, a new window will be opened, showing details about the :term:`GPS` satellites.

And even more information will be shown about the satellites used in the fix.

     |navigation-gpsrecorder-sat|

On top, in the title of the window, the number of the satellites used in the fix, vs. the total number of available ones is displayed.

GPS Recorder SPD button
"""""""""""""""""""""""

     |navigation-gpsrecorder-button-spd|

If you tap on this button, a new window will be opened, showing the current speed, as measured by the :term:`GPS`.

     |navigation-gpsrecorder-spd|

.. note:: The speed is measured in km/h by default. To change this, go to :ref:`Settings <chapters/navigation:GPS Recorder Settings>`.

GPS Recorder CELL button
""""""""""""""""""""""""

     |navigation-gpsrecorder-button-cell|

.. note:: :ref:`summary:Unexpected behavior`: This button doesn't seem to ever be active.

GPS Recorder Start/Stop button
""""""""""""""""""""""""""""""

Tapping here will start recording or, if recording is already running, it will stop it (the button text changes from *Start* to *Stop*, accordingly)

If there was nothing recorded (there was no location fix) before the recording was stopped, then the track file will be discarded (deleted), and the following message will be displayed:

     |navigation-gpsrecorder-cancel|

GPS Recorder Settings
"""""""""""""""""""""

     |navigation-gpsrecorder-settings-1|

     |navigation-gpsrecorder-settings-2|

     |navigation-gpsrecorder-log-step-1|

     |navigation-gpsrecorder-log-step-2|

     |navigation-gpsrecorder-log-step-3|

     |navigation-gpsrecorder-log-step-4|

     |navigation-unit-system|

     |navigation-gpsrecorder-speed-unit|

GPS Recorder Convert
""""""""""""""""""""




     |navigation-gpsrecorder-convert-1|

     |navigation-gpsrecorder-convert-2|

     |navigation-gpsrecorder-convert-3|

GPS Recorder About
""""""""""""""""""

CloudGPS
--------

CloudGPS is a navigation app which allows you to see your current position and movement live on an online map.


.. note:: This application is not installed by default. :ref:`chapters/appmanager:Install` it from the :ref:`chapters/appmanager:Application Manager`.

|navigation-cloudgps-main|

On the top bar of the application, there is information about:

#. **D\\l** - Data downloaded
#. **GPS fix** - Whether the position was found by :term:`GPS`, and the fix type (2D/3D)
#. **Altitude** - Altitude as measured using :term:`GPS` 3D data
#. **Speed** - Speed as measured using :term:`GPS` data
#. **Vertical bars** - Representing :term:`GPS` satellites number and signal
#. **FPS** - Frames per second, the rate of drawing the map on the screen
#. **Battery** - Device battery percentage

In the lower left and right of the screen, you have navigation buttons:

#. |navigation-cloudgps-button-zoom-out| **Magnifier -** - Zoom out
#. |navigation-cloudgps-button-zoom-in| **Magnifier +** - Zoom in
#. |navigation-cloudgps-button-3d| |navigation-cloudgps-button-2d| **3D/2D** - Toggle between flat (2D) and perspective (3D) view
#. |navigation-cloudgps-button-compass| **Compass**
#. |navigation-cloudgps-button-binoculars| **Binoculars** - Search function
#. |navigation-cloudgps-button-dot| **Blue dot** - Move focus on current location, as determined by the :term:`GPS`.

     |navigation-cloudgps-3d|

.. warning:: :ref:`summary:Known bugs`: CloudGPS application's interface seems to not behave well, appearing to have graphical errors.

     Temporary fix: enter the :ref:`chapters/ui:Power options menu`, then exit the menu, by tapping the :ref:`area around it <chapters/ui:Area around menus>`.

     Afterwards, for a very short while, you should be able to interact with the application.
