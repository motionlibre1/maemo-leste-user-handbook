 .. include:: ../chapters-img/appmanager-img.rst

Application Manager
===================

.. note:: Using this application, you can manage additional applications - the ones which are not preinstalled, or third party apps - installing, updating, and uninstalling them.

The main window looks like this:

     |app-manager-main|

You have the following options:

#. :ref:`chapters/appmanager:Uninstall`
#. :ref:`chapters/appmanager:Download` / :ref:`chapters/appmanager:Install`
#. :ref:`chapters/appmanager:Update`

Other options you can find in the :ref:`chapters/ui:In-app menu`:

     |app-manager-menu|

#. :ref:`chapters/appmanager:Application catalogs`
#. :ref:`chapters/appmanager:Log`
#. :ref:`chapters/appmanager:Restore applications`

Application catalogs
--------------------

Here are the software catalogs (repositories), from where applications can be installed.

Some are configured by default, but custom ones can also be added.

     |app-manager-catalogs|

Tapping on one of them allows you to modify it.


.. warning:: The custom catalogs can be edited, while the default ones, cannot. If you tap on a default catalog, you will receive an error message, like this one:

     |app-manager-catalog-read-only|

Tapping on **New** allows you to add a custom catalog.

     |app-manager-catalog-new|

You have the following options:

   #. **Catalog name** - The displayed name of the catalog
   #. **Web address** - The address on web where the catalog is stored
   #. **Distribution** - The Devuan version corresponding to or compatible with the installed Maemo Leste version.
   #. **Components** - The components of the catalog which should be included in the application list
   #. **Disabled** - Check to disable/deactivate this catalog (but still keep the configuration in the catalog list)

.. tip:: The application catalog entries are basically Debian-style repositiory entries, so their description and behavior are similar to those.

Log
---

Here you can see the details of application management and processing, as they would appear if ran from the terminal.

     |app-manager-log|

You also have the following options:

#. :ref:`Clear <chapters/appmanager:Clear log>`
#. :ref:`Save as <chapters/appmanager:Save log>`

Clear log
"""""""""

This deletes the previous entries in the log, emptying the text window.

Save log
""""""""

This way you can save the current log to a text file.

     |app-manager-log-export|

If desired, change the name of the file to save, instead of the default name: *Log*.

.. tip:: See :ref:`chapters/ui:Input and keyboard` about how to input text.

And tapping on **Location** allows you to save the log file in a different folder than the default (*Documents*).

     |app-manager-log-export-folder|

Navigate to the desired folder.

Tap on **New** to create a new folder at the current location.

     |app-manager-log-export-folder-new|

If desired, change the name of the file to save, from the default name: *Folder*.

Tap on **Save** to create the new folder.

And again, on **Save**, to save the log file.

Restore applications
--------------------

.. note:: :ref:`summary:Unexpected behavior`: It's not obvious what this option does, or whether it works.

Install
-------

Here you can install additional applications.

     |app-manager-install|

Tap on **All** to see all available applications, or pick a category:

   #. **Network**
   #. **Desktop**
   #. **Education**
   #. **Games**
   #. **Graphics**
   #. **Location & Navigation**
   #. **Multimedia**
   #. **Office**
   #. **System**
   #. **Utilities**

Other options you can find in the :ref:`chapters/ui:In-app menu`:

     |app-manager-menu|

#. :ref:`chapters/appmanager:Application catalogs`
#. :ref:`chapters/appmanager:Log`
#. :ref:`chapters/appmanager:Restore applications`

When you select a category, or choose to display all applications, you will be shown the corresponding list of applications available to install:

     |app-manager-install-list|

Tap on the application you want to install:

     |app-manager-install-confirm|

Check **I understand and agree** to agree with installing a third-party app, and thus accept the associated potential risks.

Tapping on **Details** shows you more information about the applications you are going to install. It will contain more tabs, each one of them containing different types of information.

     |app-manager-install-app-summary|

Details in the **Summary** tab:

   #. **Application**
   #. **Maintainer**
   #. **Status**
   #. **Category**
   #. **Available version**
   #. **Download size**
   #. **Catalog**

The **Description** tab shows the description of the application, describing what it is, what it does, and so on...

     |app-manager-install-app-description|

The **Installing** tab provides information about the the amount of storage space the application will take.

     |app-manager-install-app-size|

Close the **Details** menu, by tapping outside it (see :ref:`chapters/ui:Area around menus`).

Then tap on **Continue**, when you want to start installing the application.

It will require some steps, each one of them shown in the progress bar.

     |app-manager-install-preparation|

Preparing for installation, please wait

     |app-manager-install-app-1|

Installing 'Application'

     |app-manager-install-app-2|

Downloading xxx kB

     |app-manager-install-app-3|

Installing 'Application' (:term:`Indeterminate progress bar`)

     |app-manager-install-app-4|

Installing 'Application' (:term:`Determinate progress bar`)

     |app-manager-app-installed|

This signals that the application was successfully installed.

Tap outside this notification (see :ref:`chapters/ui:Area around notifications`) to exit.

Download
--------

This refers to downloading and installing applications, so check :ref:`chapters/appmanager:Install`.

Update
------

Here you can see whether there are updates to install for your applications.

When you enter this page manually, an automatic search for updates will be triggered.

     |app-manager-update-check|

If there are no updates available, you will see the following message: *(no updates available)*

     |app-manager-update-not-available|

If there are updates available, then you will see something like this:

     |app-manager-update|

Tap on :ref:`chapters/appmanager:Update all` to update all applications.

Other options you can find in the :ref:`chapters/ui:In-app menu`:

     |app-manager-update-menu|

#. :ref:`chapters/appmanager:Refresh`
#. :ref:`chapters/appmanager:Application catalogs`
#. :ref:`chapters/appmanager:Log`
#. :ref:`chapters/appmanager:Restore applications`

Update all
""""""""""

When you choose to update all applications, you will be shown a summary, and asked to confirm.

     |app-manager-update-all-summary|

Tap on **Continue** to start updating all applications.

You can also tap on individual applications in this list, to update only that particular application.

     |app-manager-update-confirm|

Tapping on **Details** shows you more information about the applications you are going to install. It will contain more tabs, each one of them containing different types of information.

     |app-manager-update-app-summary|

Details in the **Summary** tab:

   #. **Application**
   #. **Maintainer**
   #. **Status**
   #. **Category**
   #. **Installed version**
   #. **Size**
   #. **Available version**
   #. **Download size**
   #. **Catalog**

     |app-manager-update-size|

The **Updating** tab provides information about the amount of extra storage space the update of application will take.

Close the **Details** menu, by tapping outside it (see :ref:`chapters/ui:Area around menus`).

     |app-manager-update-confirm|

Check **I understand and agree** to agree with installing the update for a third-party app, and thus accept the associated potential risks.

Then tap on **Continue** to proceed with the installation of the update.

It will require some steps, each one of them shown in the progress bar.

     |app-manager-install-preparation|

Preparing for installation, please wait

     |app-manager-update-pending|

Updating 'Application' (:term:`Indeterminate progress bar`)

     |app-manager-update-progress|

Updating 'Application' (:term:`Determinate progress bar`)

     |app-manager-update-success|

This signals that the updates were successfully installed.

Tap outside this notification (see :ref:`chapters/ui:Area around notifications`) to exit.

Refresh
"""""""

This will manually refresh the list of acailable application versions, determining if there are new updates available.

     |app-manager-update-none|

If there are no new updates available for your installed applications, you will be shown the message *All applications are already installed*.

     |app-manager-update-prompt|

If there are new updates available for your installed applications, the *New software updates* menu will appear, offering the following options:

   #. **Ignore** - Do not install the new updates now.
   #. **Continue** - You will be taken to the :ref:`chapters/appmanager:Update` page.


Uninstall
---------

Here you can remove the already installed additional applications.

     |app-manager-uninstall|

Tap on the application which you want to uninstall.

Other options you can find in the :ref:`chapters/ui:In-app menu`:

     |app-manager-menu|

#. :ref:`chapters/appmanager:Application catalogs`
#. :ref:`chapters/appmanager:Log`
#. :ref:`chapters/appmanager:Restore applications`

You will be asked to confirm uninstalling the application, by tapping on **Uninstall**.

     |app-manager-uninstall-confirm|

But if you first want more information about the application, you can tap on **Details**. It contains more tabs, each one of them containing different types of information.

     |app-manager-uninstall-summary|

Details in the **Summary** tab:

   #. **Application**
   #. **Maintainer**
   #. **Status**
   #. **Category**
   #. **Installed version**
   #. **Size**

     |app-manager-uninstall-description|

The **Description** tab shows the description of the application, describing what it is, what it does, and so on...

     |app-manager-uninstall-size|

The **Uninstalling** tab provides information about the the amount of storage space the removal of application will free up.

Close the **Details** menu, by tapping outside it (see :ref:`chapters/ui:Area around menus`).

Then you can finally tap on **Uninstall** to proceed with the unistallation of the application.

It will require some steps, each one of them shown in the progress bar.

     |app-manager-uninstall-app-1|

Uninstalling 'Application' (:term:`Indeterminate progress bar`)

     |app-manager-uninstall-app-2|

Uninstalling 'Application' (:term:`Determinate progress bar`)

     |app-manager-uninstall-success|

This signals that the application was successfully uninstalled.

Tap outside this notification (see :ref:`chapters/ui:Area around notifications`) to exit.
